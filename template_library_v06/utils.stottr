@prefix ottr:	<http://ns.ottr.xyz/0.4/> .
@prefix rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:	<http://www.w3.org/2000/01/rdf-schema#> .
@prefix dpm:	<http://dipromag.techfak.uni-bielefeld.de/dipromag_onto/0.1/> .
@prefix xsd:	<http://www.w3.org/2001/XMLSchema#> .
@prefix owl:	<http://www.w3.org/2002/07/owl#> .
@prefix emmo:	<http://www.w3.org/2002/07/emmo> .
@prefix qb:	<http://purl.org/linked-data/cube#> .
@prefix sdmx-dimension:<http://purl.org/linked-data/sdmx/2009/dimension#> .
@prefix sdmx-measure:	<http://purl.org/linked-data/sdmx/2009/measure#> .
@prefix sdmx-concept:    <http://purl.org/linked-data/sdmx/2009/concept#> .
@prefix interval:	<http://reference.data.gov.uk/def/intervals/> .
@prefix pmdco: <https://material-digital.de/pmdco/> .

# Assigns a property of (value, unit) to an object, e.g. dpm:Has_quantity_value(?element_name, pmd:hasStandardAtomicWeight, "55.845"^^xsd:float, emmo:Dalton)
# pmdco:Object ?object is the object having the property
# ?property is the porperty the object has
# ?value measured/estimated/calculated
# ?unit is the unit of the value
dpm:HasQuantityValue[	
	?object, 
	! ottr:IRI ?property, 
	?value, 
	emmo:MeasurementUnit ?unit] :: {
		ottr:Triple(?object, emmo:hasProperty, _:b2),
		ottr:Triple(_:b2, emmo:hasMeasurementUnit, ?unit),
		ottr:Triple(_:b2, ?property, _:b3),
		ottr:Triple(_:b3, rdf:type, emmo:Real),
		ottr:Triple(_:b3, emmo:hasNumericalData, ?value)		
} .


dpm:HasQualityValue[
	?object, 
	! ottr:IRI ?property, 
	?quality_value] :: {
		ottr:Triple(?object, ?property, ?quality_value)
} .


# Can be used to create an user (template instantiator)
dpm:CreateUser[ottr:IRI ?user_name, 
		xsd:string ?name] :: {
	Triple(?user_name, rdf:type, dpm:User),
	Triple(?user_name, pmd:hasName, ?name)
} .

# Store Meta Data about an instantiation
dpm:HasMetaData[ottr:IRI ?head,
		dpm:user ?user,
		xsd:date ?date,
		xsd:string ?comment,
		xsd:boolean ?verified] :: {
	ottr:Triple(?head, dpm:instantiatedBy, ?user),
	ottr:Triple(?head, dpm:instantiatedAt, ?date),
	ottr:Triple(?head, dpm:hasComment, ?comment),
	ottr:Triple(?head, dpm:isVerified, ?verified)
	
} .

dpm:HasMetaDataAndVerification[ottr:IRI ?head,
				dpm:user ?user,
				xsd:date ?date,
				xsd:string ?comment,
				xsd:boolean ?verified] :: {
	dpm:HasMetaData(?head, ?user, ?date, ?comment),
	ottr:Triple(?head, dpm:isVerified, ?verified)
} .