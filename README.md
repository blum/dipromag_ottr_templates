# dipromag_ottr_templates

Exaple Change


== Guide ==
Click on the template you want to instantiate and click the button: "instantiate with form". Then enter a unique page name of the page: usually the IRI of the central entity of the desired template can be used. After that, you get a form you can fill out. Please use the datatypes listed below! 

Please enter dpm:IngaEnnen or dpm:MartinWortmann into the form-field "ottr:User ?user".

Datatypes:
* ottr:IRI means the template expects a unique identifier, e.g., a lab id (dpn:Fe20220517)
* xsd:float means the template expects a rational number, e.g. 5.4 or 6.63E-4
* xsd:int means the template expects a integer
* xsd:string means the template expects a text in english
* xsd:date should be formatted "YYYY-MM-DD"^^xsd:date
* xsd:time should be formatted "hh-mm-ss"^^xsd:time
* NEList<xsd:float> means the template expects a list of xsd:float, written like ("5.4"^^xsd:float, "4.78"^^xsd:float)
* NEList<xsd:string> means the template expects a list of xsd:string, written like ("test"^^xsd:float, "hello"^^xsd:float)
* and so on

Images and other files:

Files can be uploaded [https://dipromag.techfak.uni-bielefeld.de/w/index.php/Special:Upload here] (can be reached by the link "Upload file" in the "Wiki tools" menu on the left side). The link of the new created page holding the uploaded file can now be entered as ottr:IRI for files, images, ...
All unused files can be found [https://dipromag.techfak.uni-bielefeld.de/w/index.php/Special:UnusedFiles here].
 


Tutorial Slides can be found at [https://docs.google.com/presentation/d/1umP1oktBSOlD0HaP-xR4PRMwAm-HkC0aFuXYAlxREPo/edit?usp=sharing Google Drive].

Example Instantiations:
* dpm:PhysicalMeasurementEDX: [[PhysicalMeasurementEDX-NiMnCoAl13Intercallation-1]]


=== Workflows ===
Putting your data into the Wiki might involve multiple steps and and these might seem quite complex during the first runs. Therefore, the following pages describe workflows which guide you through the process of instantiating OTTR templates inside this Wiki. 

* [[Template:Thin-Film Sample Workflow]]
* [[Template:Bulk Sample Workflow]]
* [[Template:EDX Experiment Workflow]]
* [[Template:XPS Experiment Workflow]]

=== Naming Conventions ===
* Bulk Sample: "BulkSample" + \w{2} initials of the creator + YYYYMMDD + \w sample enumerator, e.g. dpm:BulkSampleLC20220524a
* Thin-Film: "ThinFilmSample" + \w{2} initials of the creator + YYYYMMDD + \w sample enumerator, e.g. dpm:ThinFilmSampleLC20220524a
* Thin-Film Layer: "ThinFilmLayer" + \w* elements: ordered by the concentration or ratio, then in descending order of the weight of the element (high weight -> low weight) + \d*4 thickness in nm + \w sample enumerator, e.g. ThinFilmLayerCo2CrAl0025a
* SampleProductionThinFilmSampleSputterParameters: "SputterParameters" + \w* elements: ordered by the concentration or ratio, then in descending order of the weight of the element (high weight -> low weight) + \w sample enumerator
* SampleProductionThinFilmSampleMBEParameters: "MBEParameters" + \w* elements: ordered by the concentration or ratio, then in descending order of the weight of the element (high weight -> low weight) + \w sample enumerator
* SampleProductionThinFilmSampleSputterSource: "SputterSource" + \w* elements: ordered by the concentration or ratio, then in descending order of the weight of the element (high weight -> low weight) + \d{2} diameter in Inch + \w sample enumerator
* SampleProductionThinFilmSampleMBESource: "MBESource" + \w* elements: ordered by the concentration or ratio, then in descending order of the weight of the element (high weight -> low weight) + \w sample enumerator
* SampleTreatmentBulkAnneal - treatment: "TreatmentAnnealBulk" + sample IRI + maximum temperature + \w enumerator
* SampleTreatmentThinFilmAnneal - treatment: "TreatmentAnnealThinFilm" + maximum temperature + \w enumerator
* SampleSynthesisBulkQuartzMelt - synthesis: "SynthesisQuartzMeltBulk" + sample IRI + maximum temperature + \w enumerator
* SampleSynthesisBulkAnneal - synthesis: "SynthesisAnnealBulk" + sample IRI + maximum temperature + \w enumerator # (melting)
* SampleSynthesisBulkArcMelt - synthesis: "SynthesisArcMeltBulk" + sample IRI + \w enumerator # (melting)
* SampleProductionBulkBallMilling - synthesis: "SynthesisBallMilling" + sample IRI + \w enumerator
* SampleProductionCrushing - treatment: "TreatmentCrushing" + sample IRI + \w enumerator
* SampleProductionBulkBallMillingJar: "BallMillingJar" + material + volume
* SampleProductionBulkPress: "SynthesisPress" + die-diameter + force
* Measurements in general: sample name + method + enumerator







* Bulk Treated Sample: sample name + \w{2} treatment (AC arc melt, AM in a tube, BM ball milled) + \w part of the sample enumerator, e.g. dpm:BulkSampleLC20220524aACa
* XPS Wide Spectrum: "XPSWideSpectrum" + sample name + \d{6} edging time in s, e.g. dpm:XPSWideSpectrumBulkSampleLC20220524a000010
* XPS Sub Spectrum: "XPSSubSpectrum" + sample name + peak identifier (e.g. Co2p, C_1s_Ru3d) + \d{2} number of the run, e.g. dpm:XPSSubSpectrumBulkSampleLC20220524aACaCo2p01
* XPS Charge Compensation: "ChargeCompensation" + \d{2}, e.g. dpm:ChargeCompensation01
* XPS Ion Etching: "IonEtching" + \d{2}, e.g. dpm:IonEtching01
* EELSPeakFitting: "EELSPeakFitting" + sample name + element + \d{2} number of the run, e.g. dpm:EELSPeakFittingBulkSampleLC20220524aCo01
