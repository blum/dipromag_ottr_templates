@prefix ottr:	<http://ns.ottr.xyz/0.4/> .
@prefix rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:	<http://www.w3.org/2000/01/rdf-schema#> .
@prefix dpm:	<http://dipromag.techfak.uni-bielefeld.de/dipromag_onto/0.1/> .
@prefix xsd:	<http://www.w3.org/2001/XMLSchema#> .
@prefix owl:	<http://www.w3.org/2002/07/owl#> .
@prefix emmo:	<http://www.w3.org/2002/07/emmo> .
@prefix qb:	<http://purl.org/linked-data/cube#> .
@prefix sdmx-dimension:<http://purl.org/linked-data/sdmx/2009/dimension#> .
@prefix sdmx-measure:	<http://purl.org/linked-data/sdmx/2009/measure#> .
@prefix sdmx-concept:    <http://purl.org/linked-data/sdmx/2009/concept#> .
@prefix interval:	<http://reference.data.gov.uk/def/intervals/> .
@prefix pmdco: <https://material-digital.de/pmdco/> .


# README
# The position in a list specifies the belonging of the element, e.g. if a pattern gets the lists element=(C, Mg) and element_count=(5, 3), it means we have C with element_count 5 and Mg with element_count 3.

# TODOs
# Mess Datum und Contributer hinzufügen
# add units
# if it makes sense, rename the parameters according to the subtemplates which uses them
# IRI Namenskonvention/ Wo fehlerhafte Nutzereingaben abfangen?
# Verbalisierungstemplate


# Use case: Not for use. 
# 

dpm:Ontology_helper[] :: {

	# compound
	ottr:Triple(dpm:Compound, rdfs:subClassOf, pmd:Material),
	ottr:Triple(dpm:Compound, rdfs:subClassOf, emmo:EngineeredMaterial),
	ottr:Triple(dpm:Off_stoichiometric_compound, rdfs:subClassOf, dpm:Compound),
	ottr:Triple(dpm:substitution_compound, rdfs:subClassOf, dpm:Compound),
	
	# shape
	ottr:Triple(dpm:compound_shape, rdfs:subClassOf, pdm:MaterialProperty),
	
	# units
	ottr:Triple(dpm:J/kgK, rdfs:subPropertyOf, emmo:SICoherentDerivedUnit),
	ottr:Triple(dpm:Gram, rdfs:subPropertyOf, emmo:SIAcceptedSpecialUnit), 



	# --- model delta_S measurements with data cubes ---
	# dimensions and measures
	# ref_magnetic_field
	ottr:Triple(dpm:ref_magnetic_field, rdf:Property, qb:DimensionProperty),
	ottr:Triple(dpm:ref_magnetic_field, rdfs:label, "reference magnetic field B"@en),
	ottr:Triple(dpm:ref_magnetic_field, rdfs:subPropertyOf, sdmx-dimension:refPeriod),
	ottr:Triple(dpm:ref_magnetic_field, rdfs:range, interval:Interval),
	ottr:Triple(dpm:ref_magnetic_field, sdmx-concept:unitMeasure, emmo:Tesla)
	# ref_temperature
	ottr:Triple(dpm:ref_temperature, rdf:Property, qb:DimensionProperty)
	ottr:Triple(dpm:ref_temperature, rdfs:label, "reference temperature T"@en),
	ottr:Triple(dpm:ref_temperature, rdfs:subPropertyOf, sdmx-dimension:refPeriod),
	ottr:Triple(dpm:ref_temperature, rdfs:range, xsd:float),
	ottr:Triple(dpm:ref_temperature, sdmx-concept:unitMeasure, dpm:B)
	ottr:Triple(dpm:ref_temperature, sdmx-concept:unitMeasure, emmo:Kelvin)
	# ref_delta_S
	ottr:Triple(dpm:ref_delta_S, rdf:Property, qb:DimensionProperty)
	ottr:Triple(dpm:ref_delta_S, rdfs:label, "measured delta S"@en),
	ottr:Triple(dpm:ref_delta_S, rdfs:subPropertyOf, sdmx-measure:obsValue;),
	ottr:Triple(dpm:ref_delta_S, rdfs:range, xsd:float),
	ottr:Triple(dpm:ref_temperature, sdmx-concept:unitMeasure, dpm:J/kgK)
	# construct cube
	ottr:Triple(dpm:sliceByMagneticField, rdf:type, qb:SliceKey), 
	ottr:Triple(dpm:sliceByMagneticField, rdf:label, "slice by megnetic field"^^xsd:string), 
	ottr:Triple(dpm:sliceByMagneticField, rdf:comment, "Slice by grouping magnetic fields together, fixing temperature values"^^xsd:string), 
	ottr:Triple(dpm:sliceByMagneticField, qb:componentProperty, dpm:ref_temperature), 
	

} .



# contains just an example how to create a datacube instance for delta_S measurements
dpm:delta_S_qb [] :: {
# --- create an instance of the datacube ---
	# data set
	ottr:Triple(dpm:dataset_delta_S_example, rdf:type, qb:DataStructureDefinition),
	ottr:Triple(dpm:dataset_delta_S_example, qb:component, _:genid1),
	ottr:Triple(dpm:dataset_delta_S_example, qb:component, _:genid2),
	ottr:Triple(dpm:dataset_delta_S_example, qb:component, _:genid3),
	ottr:Triple(dpm:dataset_delta_S_example, qb:component, _:genid4),
	ottr:Triple(_:genid1, qb:dimension, dpm:ref_magnetic_field),
	ottr:Triple(_:genid1, qb:order, "1"xsd:integer),
	ottr:Triple(_:genid2, qb:dimension, dpm:ref_temperature),
	ottr:Triple(_:genid2, qb:order, "2"xsd:integer),
	ottr:Triple(_:genid2, qb:componentAttachment, qb:Slice),
	ottr:Triple(_:genid3, qb:measure, dpm:ref_delta_S),
	ottr:Triple(_:genid4, qb:attribute, sdmx-attribute:unitMeasure),
	ottr:Triple(_:genid4, qb:componentRequired, "true"^^xsd:boolean),
	ottr:Triple(_:genid4, qb:componentAttachment, qb:DataSet),
	ottr:Triple(dpm:dataset_delta_S_example, qb:sliceKey, dpm:sliceByMagneticField),
	
	# --- put data in ---
	dpm:slice1 rdf:type qb:Slice
	dpm:slice1 qb:sliceStructure dpm:sliceByMagneticField
	dpm:slice1 dpm:ref_magnetic_field "0-1"^^xsd:float
	
	dpm:slice1 qb:observation dpm:point_1
	dpm:point_1 rdf:type qb:Observation
	dpm:point_1 qb:dataSet dpm:dataset_delta_S_example
	dpm:point_1 dpm:ref_temperature "1.5"^^xsd:float


	dpm:slice1 qb:observation dpm:point_2
	dpm:point_2 rdf:type qb:Observation
	dpm:point_2 qb:dataSet dpm:dataset_delta_S_example
	dpm:point_2 dpm:ref_temperature "1.7"^^xsd:float
	
	
	dpm:slice2 rdf:type qb:Slice
	dpm:slice2 qb:sliceStructure dpm:sliceByMagneticField
	dpm:slice2 dpm:ref_magnetic_field "0-2"^^xsd:float
	
	dpm:slice2 qb:observation dpm:point_3
	dpm:point_3 rdf:type qb:Observation
	dpm:point_3 qb:dataSet dpm:dataset_delta_S_example
	dpm:point_3 dpm:ref_temperature "1.5"^^xsd:float
} .



# just a helper template, not desired for direct user interaction	
# Createselement instances with the relevant information for a certain compound.
dpm:Bulk_compound_element_helper[	ottr:IRI ?compound_name, 
					ottr:IRI ?element, 
					xsd:float ?purity, 
					ottr:IRI ?shape, 
					xsd:float ?stoichiometry_portions, 
					xsd:float ?target_masses, 
					xsd:float ?actual_masses, 
					xsd:float ?mass_deviation]::{
	ottr:Triple(?compound_name, emmo:hasPart, _:compound_element),
	ottr:Triple(_:compound_element, emmo:hasPart, ?element),
	ottr:Triple(?element, rdf:type, emmo:Atom )
	ottr:Triple(_:compound_element, rdf:type, emmo:Material),

	# purity
	ottr:Triple(_:compound_element, dpm:purity, _:purity),
	ottr:Triple(_:purity, rdf:type, pmd:QuantityValue),
	ottr:Triple(_:purity, wikibase:quantityAmount, ?purity),
	ottr:Triple(_:purity, wikibase:quantityUnit, emmo:AmountFractionUnit),
	
	# shape
	ottr:Triple(_:compound_element, dpm:shape, ?shape),
	ottr:Triple(?shaoe, rdf:type, dpm:compound_shape),
	
	# stoichiometry
	ottr:Triple(_:compound_element, dpm:stoichiometry_portion, ?stoichiometry_portions),
	
	# masses
	ottr:Triple(_:compound_element, dpm:target_mass, _:target_masses),
	ottr:Triple(_:target_masses, rdf:type, pmd:QuantityValue),
	ottr:Triple(_:target_masses, wikibase:quantityAmount, ?target_masses),
	ottr:Triple(_:target_masses, wikibase:quantityUnit, dpm:Gram),
	
	ottr:Triple(_:compound_element, dpm:actual_mass, _:actual_mass),
	ottr:Triple(_:actual_mass, rdf:type, pmd:QuantityValue),
	ottr:Triple(_:actual_mass, wikibase:quantityAmount, ?actual_mass),
	ottr:Triple(_:actual_mass, wikibase:quantityUnit, dpm:Gram),
	
	ottr:Triple(_:compound_element, dpm:mass_deviation, _:mass_deviation),
	ottr:Triple(_:mass_deviation, rdf:type, pmd:QuantityValue),
	ottr:Triple(_:mass_deviation, wikibase:quantityAmount, ?mass_deviation),
	ottr:Triple(_:mass_deviation, wikibase:quantityUnit, dpm:Gram),
	
} .

# just a helper template, not desired for direct user interaction	
dpm:Bulk_material_substitution_operation_helper[	ottr:IRI ?material_name, 
							xsd:int ?operation_number,
							ottr:IRI ?substituted_element,
							ottr:IRI ?inserted_element] :: {
	ottr:Triple(?material_name, dpm:substitution_of, _:substitution),
	ottr:Triple(_:substitution, dpm:operation_number, ?operation_number),							
	ottr:Triple(_:substitution, dpm:substituted_element, ?substituted_elements),			
	ottr:Triple(_:substitution, dpm:inserted_element, ?inserted_elements)
} . 

# just a helper template, not desired for direct user interaction	
# Stores the fractional coordinates of an element for a lattice_instance.
dpm:Fractional_coordinates_helper [?lattice_instance, ?element, ?fractional_coordinates] :: {
	ottr:Triple(?lattice_instance, dpm:has_fractional_coordinate, _:fractional_coordinates_element),
	ottr:Triple(_:fractional_coordinates_element, dpm:fractional_coordinates_for_element, ?element),
	zipMin | dpm:Fractional_coordinate_helper(_:fractional_coordinates_element, ++(dpm:lattice_coordinate_a, dpm:lattice_coordinate_b, dpm:lattice_coordinate_c), ++?fractional_coordinates)	
} .

# just a helper template, not desired for direct user interaction	
# Stores the 4 values of a certain coordinate_dimension (a,b,c) for a fractional_coordinates_element.
dpm:Fractional_coordinate_helper [?fractional_coordinates_element, ! ?coordinate_dimension, ?values] :: {
	ottr:Triple(?fractional_coordinates_element, ?coordinate_dimension, ?values)
} .




# Use case: create a compound without modifying an existing one
# The stoichiometry of the compound is represented by a combination of ?elements and ?stoichiometry_portions. For reproducibility the ?target_masses (desired masses of the elements) and the ?actual_masses (real used masses), as well as the ?mass_deviations (difference between ?target_masses and ?actual_masses), are stored. Furthermore, for each compound element in ?elements, its purity (?purities) and shape (?shapes) is documented. In the end, the ?overall_weight of the compound is the sum of the ?actual_masses.
dpm:Bulk_material[	ottr:IRI ?material_name, 
			NEList<ottr:IRI> ?elements, 
			NEList<xsd:float> ?purities, 
			NEList<ottr:IRI> ?shapes, 
			NEList<xsd:float> ?stoichiometry_portions,
			NEList<xsd:float> ?target_masses,
			NEList<xsd:float> ?actual_masses,
			NEList<xsd:float> ?mass_deviations,
			xsd:float ?overall_weight,
			ottr:IRI ?user,
    			xsd:date ?date,
    			xsd:string ?comment] ::{
	zipMin | dpm:Bulk_compound_element_helper(?material_name, ++?elements, ++?purities, ++?shapes, ++?stoichiometry_portions, ++?target_masses, ++?actual_masses, ++?mass_deviations),
        ottr:Triple(?material_name, rdf:type, dpm:Compound),
        ottr:Triple(?material_name, dpm:weight, ?overall_weight)        
} .




# Use case: create a compound which is reached through partially substitution/doping (multiple modifications possible), or/and which is off stoichiometric
# Instantiation follows dpm:Bulk_material plus the following additional parameters. The ?source_compound is the inital compound that is modified, and ?substituted_elements and ?inserted_element together form the performed operations. If you don't remove an element, instantiate ?substituted_element at it's position with ottr:none (same if you only remove and dont't add). We allow up to 10 substitution operations. Set ?compound_type according to your compound, e.g. (dpm:Off_stoichiometric_compound, dpm:Substitution_compound) if it is off stoichiometric and a substitution compound.
dpm:Bulk_material_mod[	ottr:IRI ?material_name, 
			NEList<ottr:IRI> ?elements, 
			NEList<xsd:float> ?purities = ("0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float), 
			NEList<ottr:IRI> ?shapes = (ottr:none, ottr:none, ottr:none, ottr:none, ottr:none), 
			NEList<xsd:float> ?stoichiometry_portions,
			NEList<xsd:float> ?target_masses = ("0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float),
			NEList<xsd:float> ?actual_masses = ("0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float),
			NEList<xsd:float> ?mass_deviations = ("0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float, "0"^^xsd:float),
			xsd:float ?overall_weight, 
			List<ottr:IRI> ?compound_type = (dpm:Compound),
			ottr:IRI ?source_compound, 
			List<ottr:IRI> ?substituted_elements = (), 
			List<ottr:IRI> ?inserted_elements = (),
			ottr:IRI ?user,
    			xsd:date ?date,
    			xsd:string ?comment] ::{
	dpm:Bulk_material(?material_name, ?elements, ?purities, ?shapes, ?stoichiometry_portions, ?target_masses, ?actual_masses, ?mass_deviations, ?overall_weight),
	ottr:Triple(?material_name, rdf:type, ?compound_type),
	ottr:Triple(?material_name, dpm:based_on_compound, ?source_compound),
	zipMin | dpm:Bulk_material_substitution_operation_helper(?material_name, ++(0,1,2,3,4,5,6,7,8,9), ++?substituted_elements, ++?inserted_elements)
} .

dpm:Physical_measurement_bulk_magnetic_properties_MPMS[ottr:IRI ?compound, 
							xsd:float ?mass,
							xsd:float ?volume,
							ottr:IRI ?shape,
							ottr:IRI ?sample_orientation, 
							ottr:IRI ?holder,
							NEList<xsd:float> ?delta_T_hyst, 
							NEList<xsd:float> ?B, 
							NEList<xsd:float> ?T_c_heating, 
							NEList<xsd:float> ?T_c_cooling,  
							NEList<xsd:float> ?delta_S_max, 
							NEList<xsd:float> ?delta_B, 
							NEList<xsd:float> ?T, 
							NEList<ottr:IRI> ?heating_or_cooling,
							ottr:IRI ?delta_S_data = ottr:None,
							NEList<NEList<xsd:float>> xsd:float ?H, 
							xsd:float ?delta_T_divby_dt, 
							xsd:float ?T_step_isoH, 
							ottr:IRI ?thermal_direction_isoH, 
							NEList<xsd:float> ?T_range_isoH,
							NEList<NEList<xsd:float>> ?T_meas_isoT,
							NEList<NEList<xsd:float>> ?H_step_isoT,
							xsd:string ?thermal_direction,
							NEList<xsd:float> ?reset_T_H,
							ottr:IRI ?user,
				    			xsd:date ?date,
				    			xsd:string ?comment] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_magnetic_properties_MPMS, _:MPMS_instance),
	ottr:Triple(_:MPMS_instance, dpm:characterization_instrument_result, _:MPMS_instance_result),
	ottr:Triple(_:MPMS_instance, dpm:charactarization_instrument_settings, _:MPMS_instance_setting),
	dpm:Physical_measurement_bulk_delta_S(_:MPMS_instance_result, ?delta_S_max, ?delta_B, ?T, ?heating_or_cooling, ?delta_S_data),
	dpm:Physical_measurement_bulk_delta_T_hyst(_:MPMS_instance_result, ?delta_T_hyst, ?B, ?T_c_heating, ?T_c_cooling),
	dpm:Bulk_measurement_protocol(_:MPMS_instance_setting, ?H, ?delta_T_divby_dt, ?T_step_isoH, ?thermal_direction_isoH, ?T_range_isoH, ?T_meas_isoT, ?H_step_isoT, ?thermal_direction, ?reset_T_H)
} .



# Use case: annotate a certain instance of a compound with its lattice parameters.
# The ?Fractional_coordinates parameter gets a list of lists of lists (see example for how to input data or analyse the generated RDF tiples). All other aparameters are named accoding to their CIF name.
dpm:Cif[ottr:IRI ?material_name,
	xsd:string ?chemical_formula_structural,
	xsd:float ?cell_volume,
	xsd:float ?cell_length_a,
	xsd:float ?cell_length_b,
	xsd:float ?cell_length_c,
	xsd:float ?cell_angle_alpha,
	xsd:float ?cell_angle_beta,
	xsd:float ?cell_angle_gamma,
	NEList<ottr:IRI> ?elements,
	NEList<NEList<NEList<xsd:float>>> ?fractional_coordinates,
	xsd:string ?hermann_mauguin,
	xsd:string ?hall,
	xsd:string ?point_group,
	ottr:IRI ?crystal_syste] ::{
		# todo restructure representation
		ottr:Triple(?material_name, dpm:has_lattice, _:lattice_instance),
		ottr:Triple(_:lattice_instance, dpm:has_chemical_formula_structural, ?chemical_formula_structural),  
		ottr:Triple(_:lattice_instance, dpm:has_cell_volume, ?cell_volume),  
		ottr:Triple(_:lattice_instance, dpm:has_cell_length_a, ?cell_length_a),
		ottr:Triple(_:lattice_instance, dpm:has_cell_length_b, ?cell_length_b),
		ottr:Triple(_:lattice_instance, dpm:has_cell_length_c, ?cell_length_c),
		ottr:Triple(_:lattice_instance, dpm:has_cell_angle_alpha, ?cell_angle_alpha),
		ottr:Triple(_:lattice_instance, dpm:has_cell_angle_beta, ?cell_angle_beta),
		ottr:Triple(_:lattice_instance, dpm:has_cell_angle_gamma, ?cell_angle_gamma),
		ottr:Triple(_:lattice_instance, dpm:has_hermann_mauguin, ?hermann_mauguin),
		ottr:Triple(_:lattice_instance, dpm:has_hall, ?hall),
		ottr:Triple(_:lattice_instance, dpm:has_point_group, ?point_group),
		ottr:Triple(_:lattice_instance, dpm:has_crystal_syste, ?crystal_syste),
		zipMin | dpm:Fractional_coordinates_helper(_:lattice_instance, ++?elements, ++?fractional_coordinates)	
} .













































# XRD characterization: gets lattice information as well as parameters for the characterization instrument
dpm:Physical_measurement_bulk_structure_XRD[
	ottr:IRI ?compound,
	ottr:IRI ?shape
	xsd:float ?low_T,
	xsd:float ?high_T,
	xsd:float start_end_angle, 
	xsd:float ?lambda_source,
	low_temperature_phase_symmetry_space_group_name_H-M
	high_temperature_phase_symmetry_space_group_name_H-M
	xsd:float ?low_temperature_phase_cell_volume, (Å³)
	xsd:float ?high_temperature_phase_cell_volume, (Å³)
	xsd:float ?low_temperature_phase_cell_length_a, (Å)
	xsd:float ?high_temperature_phase_cell_length_a, (Å)
	xsd:float ?low_temperature_phase_cell_length_b, (Å)
	xsd:float ?high_temperature_phase_cell_length_b, (Å)
	xsd:float ?low_temperature_phase_cell_length_c, (Å)
	xsd:float ?high_temperature_phase_cell_length_c, (Å)
	xsd:float ?low_temperature_phase_cell_angle_alpha, (deg)
	xsd:float ?high_temperature_phase_cell_angle_alpha, (deg)
	xsd:float ?low_temperature_phase_cell_angle_beta, (deg)
	xsd:float ?high_temperature_phase_cell_angle_beta, (deg)
	xsd:float ?low_temperature_phase_cell_angle_gamma, (deg)
	xsd:float ?high_temperature_phase_cell_angle_gamma, (deg)
	?d_V_by_V, 
	NEList<ottr:IRI> ?elements,
	NEList<NEList<NEList<xsd:float>>> ?low_temperature_fractional_coordinates,
	NEList<NEList<NEList<xsd:float>>> ?high_temperature_fractional_coordinates,
	ottr:IRI ?hermann_mauguin,
	ottr:IRI ?hall,
	ottr:IRI ?point_group,
	ottr:IRI ?crystal_syste,
	ottr:IRI ?user,
	xsd:date ?date,
	xsd:string ?comment 
] :: {
		ottr:Triple(?material_name, dpm:XRD, _:XRD_instance),
		ottr:Triple(_:XRD_instance, dpm:characterization_instrument_result, _:XRD_instance_result),
		dpm:Cif(_:XRD_instance_result, ?chemical_formula_structural, ?cell_volume, ?cell_length_a, ?cell_length_b, ?cell_length_c, ?cell_angle_alpha, ?cell_angle_beta, ?cell_angle_gamma, ?elements, ?fractional_coordinates, ?hermann_mauguin, ?hall, ?point_group, ?crystal_syste),
		ottr:Triple(_:XRD_instance, dpm:charactarization_instrument_settings, _:XRD_instance_setting),
		dpm:Physical_measurement_bulk_structure_XRD_parameters(_:XRD_instance_setting, ?T, ?lambda_source, ?shape)
} . 


dpm:Physical_measurement_bulk_structure_XRD_parameters[ottr:IRI ?XRD_instance, xsd:float ?T, xsd:float ?lambda_source, ottr:IRI ?shape] :: {
	ottr:Triple(?XRD_instance, dpm:T, ?T),
	ottr:Triple(?XRD_instance, dpm:lambda_source, ?lambda_source),
	ottr:Triple(?XRD_instance, dpm:shape, ?shape)
} .
		
	
	
	
	

# todo List operations dont work with ottr:none arguments: should work now
dpm:Physical_measurement_bulk_composition_EDX[ottr:IRI ?compound, 
						NEList<ottr:IRI> ?elements, 
						NEList<xsd:float> ?stoichiometry_portion, 
						xsd:int ?number_of_spots, 
						xsd:float ?energy,
						ottr:IRI ?user,
			    			xsd:date ?date,
			    			xsd:string ?comment] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_composition_EDX, _:EDX_instance),
	ottr:Triple(_:EDX_instance, dpm:characterization_instrument_result, _:EDX_instance_result),
	ottr:Triple(_:EDX_instance, dpm:charactarization_instrument_settings, _:EDX_instance_setting),
	zipMin | dpm:Bulk_compound_element_helper(_:EDX_instance_result, ++?elements, "0"^^xsd:float, dpm:test_none, ++?stoichiometry_portion, "0"^^xsd:float,"0"^^xsd:float,"0"^^xsd:float),
	dpm:Physical_measurement_bulk_composition_EDX_parameters(_:EDX_instance_setting, ?number_of_spots, ?energy)
} .


# just a helper template, not desired for direct user interaction	
dpm:Physical_measurement_bulk_composition_EDX_parameters[ottr:IRI ?EDX_instance, 
								xsd:int ?number_of_spots, 
								xsd:float ?energy,
								ottr:IRI ?user,
					    			xsd:date ?date,
					    			xsd:string ?comment] :: {
	ottr:Triple(?EDX_instance, dpm:number_of_spots, ?number_of_spots),
	ottr:Triple(?EDX_instance, dpm:energy, ?energy)
} .


dpm:Bulk_measurement_protocol[ottr:IRI ?characterization_instance, 
				xsd:float ?H, 
				xsd:float ?delta_T, 
				xsd:float ?delta_T_divby_dt, 
				xsd:string ?thermal_direction, 
				?reset_T_H, 
				xsd:float ?T, 
				xsd:float ?delta_H, 
				xsd:float ?delta_H_divby_dt] :: {
	ottr:Triple(?characterization_instance, dpm:bulk_measurement_protocol_isofield, _:protocol_isofield_instance),
	dpm:Bulk_measurement_protocol_isofield(_:protocol_isofield_instance, ?H, ?delta_T, ?delta_T_divby_dt),
	ottr:Triple(?characterization_instance, dpm:bulk_measurement_protocol_isothermal, _:protocol_isothermal_instance),
	dpm:Bulk_measurement_protocol_isothermal(_:protocol_isothermal_instance, ?thermal_direction, ?reset_T_H, ?T, ?delta_H, ?delta_H_divby_dt)
} .


dpm:Bulk_measurement_protocol_isofield[ottr:IRI ?protocol_isofield_instance, xsd:float ?H, xsd:float ?delta_T, xsd:float ?delta_T_divby_dt] :: {
	ottr:Triple(?protocol_isofield_instance, dpm:placeholder_property, _:b)
} .


dpm:Bulk_measurement_protocol_isothermal[ottr:IRI ?protocol_isothermal_instance, xsd:string ?thermal_direction, ?reset_T_H, xsd:float ?T, xsd:float ?delta_H, xsd:float ?delta_H_divby_dt] :: {
	ottr:Triple(?protocol_isothermal_instance, dpm:placeholder_property, _:b)
} .





# todo implement example
dpm:Physical_measurement_bulk_delta_S[ottr:IRI ?physical_measurement, NEList<xsd:float> ?delta_S, NEList<xsd:float> ?delta_B, NEList<xsd:float> ?T, NEList<ottr:IRI> ?heating_or_cooling, ottr:IRI ?delta_S_data = ottr:None] :: {
	ottr:Triple(?physical_measurement, dpm:has_delta_S_max, _:delta_S_max)
} .


dpm:Physical_measurement_bulk_delta_T_hyst[ottr:IRI ?physical_measurement, NEList<xsd:float> ?delta_T_hyst, NEList<xsd:float> ?B, NEList<xsd:float> ?T_c_heating, NEList<xsd:float> ?T_c_cooling] :: {
	ottr:Triple(?physical_measurement, dpm:has_delta_T_hyst, _:delta_T_hyst)
} .


dpm:Physical_measurement_bulk_magnetic_properties_VSM[ottr:IRI ?compound, 
							xsd:float ?mass,
							xsd:float ?volume,
							ottr:IRI ?shape,
							ottr:IRI ?sample_orientation, 
							ottr:IRI ?holder,
							ottr:IRI ?magnetic_moment_component,
							NEList<xsd:float> ?delta_T_hyst, 
							NEList<xsd:float> ?B, 
							NEList<xsd:float> ?T_c_heating, 
							NEList<xsd:float> ?T_c_cooling,  
							NEList<xsd:float> ?delta_S_max, 
							NEList<xsd:float> ?delta_B, 
							NEList<xsd:float> ?T, 
							NEList<ottr:IRI> ?heating_or_cooling,
							ottr:IRI ?delta_S_data = ottr:None,
							NEList<NEList<xsd:float>> xsd:float ?H, 
							xsd:float ?delta_T_divby_dt, 
							xsd:float ?T_step_isoH, 
							ottr:IRI ?thermal_direction_isoH, 
							NEList<xsd:float> ?T_range_isoH,
							NEList<NEList<xsd:float>> ?T_meas_isoT,
							NEList<NEList<xsd:float>> ?H_step_isoT,
							xsd:string ?thermal_direction,
							NEList<xsd:float> ?reset_T_H,
							ottr:IRI ?user,
				    			xsd:date ?date,
				    			xsd:string ?comment] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_magnetic_properties_VSM, _:VSM_instance),
	ottr:Triple(_:VSM_instance, dpm:characterization_instrument_result, _:VSM_instance_result),
	ottr:Triple(_:VSM_instance, dpm:charactarization_instrument_settings, _:VSM_instance_setting),
	dpm:Physical_measurement_bulk_delta_S(_:VSM_instance_result, ?delta_S_max, ?delta_B, ?T, ?heating_or_cooling, ?delta_S_data),
	dpm:Physical_measurement_bulk_delta_T_hyst(_:VSM_instance_result, ?delta_T_hyst, ?B, ?T_c_heating, ?T_c_cooling),
	dpm:Bulk_measurement_protocol(_:VSM_instance_setting, ?H, ?delta_T_divby_dt, ?T_step_isoH, ?thermal_direction_isoH, ?T_range_isoH, ?T_meas_isoT, ?H_step_isoT, ?thermal_direction, ?reset_T_H)
} .


dpm:Physical_measurement_bulk_heat_capacity_DSC-zero-field[ottr:IRI ?compound, 
								ottr:IRI ?shape, 
								xsd:int ?heat_cycles_number, 
								NEList<xsd:float> ?delta_T_hyst, 
								NEList<xsd:float> ?B, 
								NEList<xsd:float> ?T_c_heating, 
								NEList<xsd:float> ?T_c_cooling,  
								xsd:float ?delta_T_divby_dt,
								xsd:float latent_heat_heating,
								xsd:float latent_heat_cooling,
								ottr:IRI ?atmosphere,
								xsd:float ?gas_flow,
								xsd:float ?sample_mass,
								ottr:IRI ?user,
					    			xsd:date ?date,
					    			xsd:string ?comment
								] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_heat_capacity_DSC, _:DSC_instance),
	ottr:Triple(_:DSC_instance, dpm:characterization_instrument_result, _:DSC_instance_result),
	ottr:Triple(_:DSC_instance, dpm:charactarization_instrument_settings, _:DSC_instance_setting),
	dpm:Physical_measurement_bulk_delta_T_hyst(_:DSC-zero-field_instance_result, ?delta_T_hyst, (0), ?T_c_heating, ?T_c_cooling)
} .


dpm:Physical_measurement_bulk_heat_capacity_DSC-in-field[ottr:IRI ?compound, 
								NEList<xsd:float> ?delta_T_hyst, 
								NEList<xsd:float> ?B, 
								NEList<xsd:float> ?T_c_heating, 
								NEList<xsd:float> ?T_c_cooling,  
								NEList<xsd:float> ?delta_S, 
								NEList<xsd:float> ?delta_B, 
								NEList<xsd:float> ?T, 
								NEList<ottr:IRI> ?heating_or_cooling, 
								ottr:IRI ?delta_S_data = ottr:None,
								NEList<xsd:float> ?d_T_ad, 
								xsd:float ?d_T,
								xsd:float ?d_B,
								ottr:IRI ?heating_or_cooling,
								xsd:float latent_heat_and_H, 
								ottr:IRI ?atmosphere, 
								xsd:float ?gas_flow, 
								xsd:float ?sample_mass,
								ottr:IRI ?user,
					    			xsd:date ?date,
					    			xsd:string ?comment] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_heat_capacity_DSC-in-field, _:DSC_instance),
	ottr:Triple(_:DSC_instance, dpm:characterization_instrument_result, _:DSC_instance_result),
	ottr:Triple(_:DSC_instance, dpm:charactarization_instrument_settings, _:DSC_instance_setting),
	dpm:Physical_measurement_bulk_delta_T_hyst(_:DSC-in-field_instance_result, ?delta_T_hyst, ?B, ?T_c_heating, ?T_c_cooling),
	dpm:Physical_measurement_bulk_delta_S(_:DSC-in-field_instance_result, ?delta_S_max, ?delta_B, ?T, ?heating_or_cooling, ?delta_S_data),
} .


dpm:Physical_measurement_bulk_transport_properties_MPMS-ETO[ottr:IRI ?compound, 
								NEList<xsd:float> ?delta_T_hyst, 
								NEList<xsd:float> ?B, 
								NEList<xsd:float> ?T_c_heating, 
								NEList<xsd:float> ?T_c_cooling,
								ottr:IRI ?user,
					    			xsd:date ?date,
					    			xsd:string ?comment] :: {
ottr:Triple(?compound, dpm:physical_measurement_bulk_transport_properties_MPMS-ETO, _:MPMS-ETO_instance),
	ottr:Triple(_:MPMS-ETO_instance, dpm:characterization_instrument_result, _:MPMS-ETO_instance_result),
	ottr:Triple(_:MPMS-ETO_instance, dpm:charactarization_instrument_settings, _:MPMS-ETO_instance_setting),
	zipMin | dpm:Physical_measurement_bulk_delta_T_hyst(_:MPMS-ETO_instance_result, ++?delta_T_hyst, ++?B, ++?T_c_heating, ++?T_c_cooling)
} .

dpm:Physical_measurement_bulk_heat_capacity_PPMS[ottr:IRI ?compound, 
							NEList<xsd:float> ?delta_T_hyst, 
							NEList<xsd:float> ?B, 
							NEList<xsd:float> ?T_c_heating, 
							NEList<xsd:float> ?T_c_cooling,  
							NEList<xsd:float> ?delta_S, 
							NEList<xsd:float> ?delta_B, 
							NEList<xsd:float> ?T, 
							xsd:float ?delta_T_ad,
							ottr:IRI ?user,
				    			xsd:date ?date,
				    			xsd:string ?comment] :: {
	ottr:Triple(?compound, dpm:physical_measurement_bulk_heat_capacity_PPMS, _:PPMS_instance),
	ottr:Triple(_:PPMS_instance, dpm:characterization_instrument_result, _:PPMS_instance_result),
	ottr:Triple(_:PPMS_instance, dpm:charactarization_instrument_settings, _:PPMS_instance_setting),
	zipMin | dpm:Physical_measurement_bulk_delta_T_hyst(_:PPMS_instance_result, ++?delta_T_hyst, ++?B, ++?T_c_heating, ++?T_c_cooling),
	zipMin | dpm:Physical_measurement_bulk_delta_S(_:PPMS_instance_result, ++?delta_S, ++?delta_B, ++?T),
	ottr:Triple(_:PPMS_instance_setting, dpm:delta_T_ad, ?delta_T_ad)
} .















