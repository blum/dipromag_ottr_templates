@prefix ottr:	<http://ns.ottr.xyz/0.4/> .
@prefix rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:	<http://www.w3.org/2000/01/rdf-schema#> .
@prefix dpm:	<http://dipromag.techfak.uni-bielefeld.de/dipromag_onto/0.1/> .
@prefix xsd:	<http://www.w3.org/2001/XMLSchema#> .
@prefix owl:	<http://www.w3.org/2002/07/owl#> .
@prefix emmo:	<http://www.w3.org/2002/07/emmo> .
@prefix qb:	<http://purl.org/linked-data/cube#> .
@prefix sdmx-dimension:<http://purl.org/linked-data/sdmx/2009/dimension#> .
@prefix sdmx-measure:	<http://purl.org/linked-data/sdmx/2009/measure#> .
@prefix sdmx-concept:    <http://purl.org/linked-data/sdmx/2009/concept#> .
@prefix interval:	<http://reference.data.gov.uk/def/intervals/> .
@prefix pmdco: <https://material-digital.de/pmdco/> .

# === Synthesis ===


# Central representation of a bulk compound sample: Measurements, manufacturing processes, etc. are referring to this certain compound instantiation. It holds the most relevant information required to describe a compound that is not created by modifying an existing one. All parameter values are given by construction and no measurements or estimations of values are required.
# ?elements, ?purities, ?shapes, stoichiometry_portions, ?target_masses,?actual_masses, ?mass_deviations are lists where elements at the same index position belong together.
# * The stoichiometry of the compound is represented by a combination of ?elements and ?stoichiometry_portions.  
# * For each compound element in ?elements, its purity (?purities) and shape (?shapes) is documented.
# * For reproducibility the ?target_masses (desired masses of the elements) and the ?actual_masses (real used masses), as well as the ?mass_deviations (difference between ?target_masses and ?actual_masses), are stored. 
# * The ?overall_weight of the compound is the sum of the ?actual_masses.
# * ?user, ?date, ?comment are metadata about the the template instantiation: fill with dpm:Yourname, YYYY-MM-DD^^xsd:date, "your comment"^^xsd:string
# Use case: represent a compound without modifying an existing compound.
dpm:SampleBulk[
	ottr:IRI ?sample, 
	xsd:string ?sample_code,
	dpm:Compound ?intended_compound,
	NEList<dpm:Element> ?elements, 
	NEList<xsd:float> ?purities_unit_percent, 
	NEList<dpm:Shape> ?shapes, 
	NEList<xsd:float> ?target_masses_unit_kg,
	NEList<xsd:float> ?actual_masses_unit_kg,
	NEList<xsd:float> ?mass_deviations_unit_kg,
	xsd:float ?overall_weight_unit_kg,
	NEList<pmdco:Process> ?production_steps,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] ::{
    	ottr:Triple(?sample, rdf:type, dpm:SampleBulkCompound),
    	ottr:Triple(?sample, rdf:type, ?intended_compound),
		ottr:Triple(?sample, rdfs:label, ?sample_code),    
		ottr:Triple(?sample, dpm:labId, ?sample_code),
		zipMin | dpm:SampleBulkElementHelper(?sample, ++?elements, ++?purities_unit_percent, ++?shapes, ++?target_masses_unit_kg, 		++?actual_masses_unit_kg, ++?mass_deviations_unit_kg),
		dpm:HasQuantityValue(?sample, pmd:hasOverallWeight, ?overall_weight_unit_kg, emmo:Kilogram)
		dpm:HasMetaDataAndVerification(?sample, ?user, ?date, ?comment, ?verified)
} .


dpm:SampleProductionCrushing[
	ottr:IRI ?ball_milling_name, 
	dpm:Sample ?bulk_sample, 
	xsd:float ?sample_mass_in_unit_kg,
	ottr:IRI ?jar, 
	xsd:float ?ball_diameter_unit_m,
	dpm:Sample ?ball_material,
	xsd:float ?sample_to_ball_mass_ratio_unit_ratio, 
	pmdco:Material ?sealing_atmosphere,
	xsd:float ?speed_unit_ratations_divby_minutes,
	NEList<xsd:float> ?milling_break_cycles_unit_s_and_s_and_number,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?ball_milling_name, ?user, ?date, ?comment)
} .

# This is melting
dpm:SampleSynthesisBulkAnneal[	
	ottr:IRI ?anneal_name,
	xsd:float ?mass_in_unit_kg,
	xsd:float ?mass_out_unit_kg,
	xsd:string ?sample_container="",
	pmdco:Material ?atmosphere,
	sd:float ?pressure_unit_pascal,
	List<List<xsd:float>> ?ramp_unit_K_and_K_and_K_divby_s_or_s,
	xsd:string ?quench="",
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?anneal_name, ?user, ?date, ?comment)
} .


dpm:SampleSynthesisBulkQuartzMelt[
	ottr:IRI ?quartz_melt_name,
	ottr:IRI ?annealing_or_melting,
	xsd:float ?mass_in_unit_kg,
	xsd:float ?mass_out_unit_kg,
	xsd:string ?sample_container="",
	pmdco:Material ?gas,
	xsd:float ?pressure_unit_pascal,
	List<List<xsd:float>> ?ramp_unit_K_and_K_and_K_divby_s_or_s,
	xsd:string ?quench="",
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?quartz_melt_name, ?user, ?date, ?comment)
} .





dpm:SampleSynthesisBulkArcMelt[
	ottr:IRI ?arc_melt_name,
	xsd:float ?mass_in_unit_kg,
	xsd:float ?mass_out_unit_kg,
	xsd:float ?vacuum_unit_pascal,
	pmdco:Material ?atmosphere,
	xsd:float ?current_unit_A,
	xsd:int ?number_of_meltings_unit_number,
	xsd:string ?crucible,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?arc_melt_name, ?user, ?date, ?comment)
} .

dpm:SampleThinFilmSample[	
	ottr:IRI ?tf_sample, 
	xsd:string ?sample_name,
	xsd:string ?lab_id,
	NEList<dpm:SampleThinFilmLayer> ?tf_layer_type,
	NEList<dpm:SamplehinFilmLayer> ?tf_layer_instance,		
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] ::{
    	ottr:Triple(?tf_sample, rdf:type, dpm:SampleThinFilm),
		ottr:Triple(?tf_sample, rdfs:label, ?sample_name),    
		ottr:Triple(?tf_sample, dpm:labId, ?lab_id),
    	zipMin | ottr:Triple(?tf_sample, emmo:hasPart, ++?tf_layer_instance),
    	zipMin | ottr:Triple(++?tf_layer_instance, rdf:type, ++?tf_layer_type),
		dpm:HasMetaDataAndVerification(?tf_sample, ?user, ?date, ?comment, ?verified)
} .


dpm:SampleThinFilmSampleLayer[	dpm:IRI ?tf_layer,
	dpm:Compound ?intended_compound,
	emmo:Parameter ?parameter,
	?tf_thickness_nominal_unit_m,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] ::{
		ottr:Triple(?tf_layer, rdfs:label, ?sample_name),   
		ottr:Triple(?tf_layer, rdf:type, ?intended_compound), 
		dpm:HasQuantityValue(?tf_layer, dpm:ThicknessNominal, ?tf_thickness_nominal_unit_m, dpm:Metre),
		ottr:Triple(?parameter, rdf:type, emmo:Parameter),
		ottr:Triple(?tf_layer, dpm:hasParameter, ?parameter),
		dpm:HasMetaDataAndVerification(?tf_layer, ?user, ?date, ?comment, ?verified)
} .


dpm:SampleThinFilmSampleSubstrateLayer[	ottr:IRI ?substrate,
	dpm:Compound ?intended_compound,
	xsd:float ?substrate_dimensions_unit_m,
	xsd:float ?oxid_layer_thickness_unit_m,
	xsd:float ?substrat_layer_thickness_unit_m,
	NEList<xsd:int> ?substrate_orientation,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] ::{
    	ottr:Triple(?substrate, rdf:type, dpm:SampleThinFilmLayerSubstrat),
		ottr:Triple(?substrate, rdfs:label, ?sample_name),    
		ottr:Triple(?substrate, rdf:type, ?intended_compound),
		dpm:HasQuantityValue(?substrate, pmd:SubstratDimensions, ?substrate_dimensions_unit_m, emmo:Metre),
		dpm:HasQualityValue(?substrate, dpm:Orientation, ?substrate_orientation),   
		dpm:HasMetaDataAndVerification(?substrate, ?user, ?date, ?comment, ?verified)
} .


dpm:SampleProductionThinFilmSampleSputterParameters[	ottr:IRI  ?sputter_parameter_name,
							NEList<ottr:IRI> ?sources,
							xsd:float ?substrate_temperature_during_decomposition_unit_K="300.0"^^xsd:float,
							pmdco:Material ?process_gas = dpm:Ar,
							xsd:float ?process_pressure_unit_pascal,
							xsd:float ?base_pressure_unit_pascal,
							xsd:boolean ?homogeneity-mechanism_used,
							dpm:Orientation ?magnetfield_orientation, 
							xsd:float ?fieldstrength_unit_T,
							NEList<ottr:IRI> ?generator_type,
							NEList<xsd:float> ?mag_field_strength_above_target_unit_T,
							NEList<xsd:float> ?source_tilt_angle_unit_deg, 
							NEList<xsd:float> ?distance_to_sample_unit_m,
							NEList<xsd:float> ?power_unit_W, 
							xsd:time ?initial_presputter_time_unit_s,
							xsd:float ?expected_deposition_rate_unit_angstroem_divby_s,
							xsd:time ?deposition_time_unit_s,
							dpm:User ?user,
    							xsd:date ?date,
    							xsd:string ?comment=""^^xsd:string,
    							xsd:boolean ?verified] ::{
    	ottr:Triple(?sputter_parameter_name, rdf:type, dpm:SputterParameter),
    	zipMin | dpm:QualityValue(?sputter_parameter_name, dpm:Target, ++?targets),
    	dpm:HasQuantityValue(?sputter_parameter_name, pmd:SubstrateTemperature, ?substrate_temperature_unit_K, emmo:Kelvin),
    	dpm:HasQualityValue(?sputter_parameter_name, dpm:ProcessGas, ?process_gas),	
    	dpm:HasQuantityValue(?sputter_parameter_name, pmd:ProcessPressure, ?process_pressure_unit_pascal, emmo:Pascal),	
    	dpm:HasQuantityValue(?sputter_parameter_name, pmd:BasePressure, ?base_pressure_unit_pascal, emmo:Pascal),	
    	dpm:HasQuantityValue(?sputter_parameter_name, pmd:HomogeneityMechanism, ?homogeneity, dpm:None),	
    	dpm:HasQualityValue(?sputter_parameter_name, dpm:Orientation, ?magnetfield_orientation-mechanism_used),
    	dpm:HasQuantityValue(?sputter_parameter_name, pmd:Fieldstrength, ?fieldstrength_unit_T, dpm:Tesla),	
    	dpm:HasQualityValue(?target_name, dpm:Generator, ?generator_type),
	dpm:HasQuantityValue(?target_name, dpm:MagFieldStrengthAboveTarget, ?mag_field_strength_above_target_unit_T, emmo:Tesla),
	dpm:HasQuantityValue(?target_name, dpm:SourceTiltAngle, ?source_tilt_angle_unit_deg, emmo:Degree),
	dpm:HasQuantityValue(?target_name, dpm:DistanceToSample, ?distance_to_sample_unit_m, emmo:Metre),
	dpm:HasQuantityValue(?target_name, dpm:Power, ?power_unit_W, emmo:Watt),
	dpm:HasQuantityValue(?target_name, dpm:InitialPresputterTime, ?initial_presputter_time_unit_s, emmo:Second),
	dpm:HasQuantityValue(?target_name, dpm:DepositionRate, ?expected_deposition_rate_unit_angstroem_divby_s, dpm:Angstroem_divby_s),
	dpm:HasQuantityValue(?target_name, dpm:DepositionTime, ?deposition_time_unit_s, emmo:Second),
    	dpm:HasMetaDataAndVerification(?sputter_parameter_name, ?user, ?date, ?comment, ?verified)
} .


dpm:SampleProductionThinFilmSampleMBEParameters[	ottr:IRI  ?mbe_parameter_name, 
							NEList<ottr:IRI> ?sources,
							xsd:float ?substrate_temperature_unit_K,
							xsd:float ?process_pressure_unit_pascal, 
							xsd:float ?base_pressure_unit_pascal,
							xsd:float ?source_temperature_unit_K,
							xsd:float ?distance_to_sample_unit_m,
							xsd:float ?deposition_rate_angstroem_divby_s,
							xsd:time ?deposition_time_unit_s,
							NEList<xsd:int> ?tilt_angle_source_unit_deg,
							dpm:User ?user,
    							xsd:date ?date,
    							xsd:string ?comment=""^^xsd:string,
    							xsd:boolean ?verified] ::{
    	ottr:Triple(?mbe_parameter_name, rdf:type, dpm:MbeParameter),	
    	zipMin | dpm:HasQualityValue(?mbe_parameter_name, dpm:Target, ++?multiple_targets),
    	dpm:HasQuantityValue(?mbe_parameter_name, pmd:SubstrateTemperature, ?substrate_temperature_unit_K, emmo:Kelvin),		
    	dpm:HasQuantityValue(?mbe_parameter_name, pmd:hasProcessPressure, ?process_pressure_unit_pascal, emmo:Pascal),	
    	dpm:HasQuantityValue(?mbe_parameter_name, pmd:hasBasePressure, ?base_pressure_unit_pascal, emmo:Pascal),	
    	dpm:HasMetaDataAndVerification(?mbe_parameter_name, ?user, ?date, ?comment, ?verified)
} .

dpm:SampleProductionThinFilmSampleSputterSource[	ottr:IRI ?source_name,
							dpm:Compound ?source_compound,
							xsd:float ?target_purity_unit_percent,
							xsd:float ?target_diameter_unit_m,
							dpm:User ?user,
    							xsd:date ?date,
    							xsd:string ?comment=""^^xsd:string] ::{
      	ottr:Triple(?target_name, rdf:type, dpm:SputterSourceParameter),	
      	ottr:Triple(?target_name, rdf:type, dpm:Sample),	
	dpm:HasQuantityValue(?target_name, dpm:Purity, ?target_purity_unit_percent, emmo:Percent),
	dpm:HasQuantityValue(?target_name, dpm:Diameter, ?target_diameter_unit_m, emmo:Metre),
	dpm:HasMetaDataAndVerification(?target_name, ?user, ?date, ?comment, ?verified)
	
		
} .



dpm:SampleProductionThinFilmSampleMBESource[	ottr:IRI ?source_name,
						dpm:Compound ?source_compound,
						xsd:float ?source_purity_unit_percent,
						dpm:User ?user,
    						xsd:date ?date,
    						xsd:string ?comment=""^^xsd:string] ::{
    	ottr:Triple(?source_name, rdf:type, dpm:MBESourceParameter),	
  	dpm:HasQuantityValue(?source_name, dpm:Purity, ?source_purity_unit_percent, emmo:Percent),
  	dpm:HasMetaData(?source_name, ?user, ?date, ?comment)
} .



# === Treatment ===



dpm:SampleProductionTreatedSample[
	ottr:IRI ?output_sample,
	dpm:Sample ?input_sample,
	NEList<ottr:IRI> ?treatments,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] :: {
		dpm:HasMetaDataAndVerification(?annealing_name, ?user, ?date, ?comment, ?verified)  
} .

dpm:SampleProductionBulkBallMilling[	
	ottr:IRI ?ball_milling_name, 
	dpm:Sample ?bulk_sample, 
	xsd:float ?sample_mass_in_unit_kg,
	ottr:IRI ?jar, 
	xsd:float ?ball_diameter_unit_m,
	dpm:Sample ?ball_material,
	xsd:float ?sample_to_ball_mass_ratio_unit_ratio, 
	pmdco:Material ?sealing_atmosphere,
	xsd:float ?speed_unit_ratations_divby_minutes,
	NEList<xsd:float> ?milling_break_cycles_unit_s_and_s_and_number,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?ball_milling_name, ?user, ?date, ?comment)
} .

dpm:SampleProductionBulkBallMillingJar[
	ottr:IRI ?jar_name, 
	xsd:float ?jar_volume_unit_m3,
	dpm:Sample ?jar_material,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?jar_name, ?user, ?date, ?comment)
} .


dpm:SampleProductionBulkPress[	
	ottr:IRI ?press_name,
	sd:float ?force_unit_kg,
	xsd:float ?die-diameter_unit_m,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?press_name, ?user, ?date, ?comment)
} .


# This is just annealing
dpm:SampleTreatmentBulkAnneal[	
	ottr:IRI ?anneal_name,
	xsd:float ?mass_in_unit_kg,
	xsd:float ?mass_out_unit_kg,
	xsd:string ?sample_container="",
	pmdco:Material ?atmosphere,
	xsd:float ?pressure_unit_pascal,
	List<List<xsd:float>> ?ramp_unit_K_and_K_and_K_divby_s_or_s,
	xsd:string ?quench="",
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string] ::{
		dpm:HasMetaData(?anneal_name, ?user, ?date, ?comment)
} .

dpm:SampleTreatmentThinFilmAnneal[	
	ottr:IRI ?annealing_name,
	pmdco:Material ?atmosphere,
	xsd:float ?pressure_unit_pascal,
	List<xsd:float> ?dwell_time_unit_s, # time the temperature is held
	List<xsd:float> ?ramp_unit_K, # end temperature
	xsd:boolean ?vacuum_break_next_machine="false"^^xsd:boolean,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string,
    xsd:boolean ?verified] ::{
    	ottr:Triple(?annealing_name, rdf:type, dpm:Annealing),
    	dpm:HasQualityValue(?annealing_name, dpm:Atmosphere, ?atmosphere),	
    	dpm:HasQuantityValue(?annealing_name, pmd:Pressure, ?pressure_unit_pascal, emmo:Pascal),	
    	dpm:HasQuantityValue(?annealing_name, pmd:Ramp, ?ramp_unit_K, emmo:Kelvin),	
    	dpm:HasQuantityValue(?annealing_name, pmd:VacuumBreak, ?vacuum_break_next_machine, dpm:None),	
    	dpm:HasMetaDataAndVerification(?annealing_name, ?user, ?date, ?comment, ?verified)    						
} .