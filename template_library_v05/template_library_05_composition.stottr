@prefix ottr:	<http://ns.ottr.xyz/0.4/> .
@prefix rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:	<http://www.w3.org/2000/01/rdf-schema#> .
@prefix dpm:	<http://dipromag.techfak.uni-bielefeld.de/dipromag_onto/0.1/> .
@prefix xsd:	<http://www.w3.org/2001/XMLSchema#> .
@prefix owl:	<http://www.w3.org/2002/07/owl#> .
@prefix emmo:	<http://www.w3.org/2002/07/emmo> .
@prefix qb:	<http://purl.org/linked-data/cube#> .
@prefix sdmx-dimension:<http://purl.org/linked-data/sdmx/2009/dimension#> .
@prefix sdmx-measure:	<http://purl.org/linked-data/sdmx/2009/measure#> .
@prefix sdmx-concept:    <http://purl.org/linked-data/sdmx/2009/concept#> .
@prefix interval:	<http://reference.data.gov.uk/def/intervals/> .
@prefix pmdco: <https://material-digital.de/pmdco/> .

# Can be used to create Element instances which are used later in most other templates as parameter values.
# ?element_name should be a unique identifier, e.g. dpm:Element_Fe
# ?name is the name of the element as text, e.g. "Iron"
# ?acronym is the acronym as text, e.g. "Fe"
dpm:CreateElement[	ottr:IRI ?element_name, 
		xsd:string ?name="", 
		xsd:string ?acronym="", 
		xsd:float standard_atomic_weight_unit_Da] :: {
	ottr:Triple(?element_name, rdf:type, dpm:Element),
	ottr:Triple(?element_name, rdfs:label, ?name),
	ottr:Triple(?element_name, rdfs:label, ?acronym),	
	dpm:HasQuantityValue(?element_name, pmd:hasStandardAtomicWeight, "55.845"float, emmo:Dalton)
} .

# General: (What data does this template hold? + Example use case.)
# Specification of an idealized material.

# Independent Parameters: (Explain all parameters which are independent of other templates.)
# ?elements and ?stoichiometry_portion_unit_at_percent belong together, and specify the atomic percentage of each element in the ideal material. 
# ?method_of_derivation (e.g. dpm:Doping) specifies how ?base_material is modified to derive the considered material. If the material is not derived, both parameters are set to ottr:None.
# ?user, ?date, ?comment, ?verified is the metadata. 

# Template Relations: (Explain all parameters which depend on other templates and how these are related.)
# ?elements must be defined by the dpm:Element template before using them here.

dpm:Composition[	
	ottr:IRI ?compound, 
	NEList<dpm:Element> ?elements,
	NEList<xsd:float> ?stoichiometry_portion_unit_at_percent,
	dpm:User ?user,
    xsd:date ?date,
    xsd:string ?comment=""^^xsd:string]::{
		ottr:Triple(?compound, emmo:hasPart, ++?elements),
		ottr:Triple(?compound, rdf:type, dpm:Compound),
		dpm:HasMetaData(?compound, ?user, ?date, ?comment),
		zipMin | dpm:CompsiteElementHelper(?compound, ++?elements, ++?stoichiometry_portion_unit_at_percent)
} .